Make sure you have database **intro_app**
 
```CREATE DATABASE `intro_app`;```


It is important to set up Doctrine database connection in project *app/config/config.yml*



To create tables associated with Doctrine Entity, run following command in project root:

```php app/console doctrine:schema:update --force```