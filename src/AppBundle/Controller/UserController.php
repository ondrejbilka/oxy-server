<?php

namespace AppBundle\Controller;


use AppBundle\Exceptions\UserValidationException;
use AppBundle\Exceptions\InvalidConstraintInstanceException;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\ConstraintViolationList;

use AppBundle\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class UserController extends FOSRestController
{

    /**
     * GET - get list of users
     *
     * @Rest\Get("/user")
     * @Rest\Get("/user/")
     *
     */
    public function getAllAction()
    {
        $result = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

        if (empty($result)) {
            return $this->response(Response::HTTP_NO_CONTENT, [], 'There are no users exist');
        }

        return $this->response(Response::HTTP_OK, $result);
    }

    /**
     * GET - get user detail
     *
     * @Rest\Get("/user/{id}")
     * @param $id
     * @return View|object
     */
    public function getByIdAction($id)
    {
        $result = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        if(empty($result)){
            return $this->response(Response::HTTP_NOT_FOUND, [], 'User ID not found', 1);
        }

        return $this->response(Response::HTTP_OK, $result);
    }

    /**
     * POST - Create new user
     *
     * @Rest\Post("/user")
     * @Rest\Post("/user/")
     *
     * @param Request $request
     * @return View
     */
    public function postToCreateAction(Request $request)
    {
        $name = $request->get('name');
        $password = $request->get('password');
        $email = $request->get('email');
        $role = $request->get('role');

        try{
            $this->validate('name', $name);
            $this->validate('password', $password);
            $this->validate('email', $email);
            $this->validate('role', $role);
        }
        catch(UserValidationException $validationException){
            return $this->response(Response::HTTP_NOT_ACCEPTABLE, [], $validationException->getMessage(), 1);
        }

        $user = new User();
        $user->setName($name);
        $user->setPassword( sha1($password) );
        $user->setEmail($email);
        $user->setRole($role);

        $manager = $this->getDoctrine()->getManager();
        try{
            $manager->persist($user);
            $manager->flush();
        }catch(UniqueConstraintViolationException $ex){
            return $this->response(Response::HTTP_NOT_ACCEPTABLE, [], 'email: Used email address already exists.', 1);
        }

        return $this->response(Response::HTTP_OK, ['id' => $user->getId()], 'User successfully added.');
    }


    /**
     * PUT - update existing user
     *
     * @Rest\Put("/user/{id}")
     *
     * @param $id
     * @param Request $request
     * @return View
     */
    public function putToEditAction($id, Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        if( empty($user) )
        {
            return $this->response(Response::HTTP_NOT_FOUND, [], 'User ID not found', 1);
        }

        $name = $request->get('name');
        $password = $request->get('password');
        $email = $request->get('email');
        $role = $request->get('role');
        
        
        try{
            if( !is_null($name) ){
                $this->validate('name', $name);
                $user->setName($name);
            }

            if( !is_null($password) ){
                $this->validate('password', $password);
                $user->setPassword(sha1($password));
            }

            if( !is_null($email) ){
                $this->validate('email', $email);
                $user->setEmail($email);
            }

            if( !is_null($role) ){
                $this->validate('role', $role);
                $user->setRole($role);
            }
            

            $manager->flush();

        }catch(UserValidationException $validationException){
            return $this->response(Response::HTTP_NOT_ACCEPTABLE, [], $validationException->getMessage(), 1);
        }catch(UniqueConstraintViolationException $uniqueConstraintException)
        {
            return $this->response(Response::HTTP_NOT_ACCEPTABLE, [], 'email: Used email address already exists.', 1);
        }

        return $this->response(Response::HTTP_NO_CONTENT, []);
    }


    /**
     * DELETE - delete existing user
     *
     * @Rest\Delete("/user/{id}")
     *
     * @param $id

     * @return View
     */
    public function deleteByIdAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        if( empty($user) )
        {
            return $this->response(Response::HTTP_NOT_FOUND, [], 'User ID not found', 1);
        }

        $manager->remove($user);
        $manager->flush();

        return $this->response(Response::HTTP_OK, [], 'User successfully deleted');
    }

    /**
     * Unified response helper
     *
     * @param $code
     * @param $content
     * @param string $message
     * @param int $error
     * @return View
     */
    public function response($code, $content, $message = '', $error = 0)
    {
        $response = [
            'content' => $content,
            'message' => $message,
            'error' => $error
        ];

        return new View ($response, $code);
    }

    /**
     * Set of validation rules
     *
     * @param $field
     * @return array|mixed
     */
    public function getValidationRulesByField($field)
    {
        $rules = [
            'name' => [ new NotBlank() ],
            'password' => [ new NotBlank(), new Length([ 'min' => 6 ]) ],
            'email' => [ new NotBlank(), new EmailConstraint() ],
            'role' => [ new NotBlank(), new Choice( User::getRoles() ) ]
        ];

        if( isset($rules[$field]) ){
            return $rules[$field];
        }

        return [];
    }

    /**
     * Custom field validator
     *
     * @param $field
     * @param $content
     * @return array
     *
     * @throws InvalidConstraintInstanceException
     * @throws UserValidationException
     */
    protected function validate($field, $content)
    {
        $constraints = $this->getValidationRulesByField($field);
        if(!empty($constraints))
        {
            foreach($constraints as $constraint)
            {
                if( ! ($constraint instanceof Constraint) )
                {
                    throw new InvalidConstraintInstanceException('Used validation constraints must be instance of Constraint class');
                }
            }
        }

        /** @var $violations ConstraintViolationList */
        $violations = $this->get('validator')->validate($content, $constraints);

        $errors = [];
        if(0 !== count($violations))
        {
            foreach($violations as $violation)
            {
                $errors[] = $field . ": " . $violation->getMessage();
            }
            throw new UserValidationException(implode(', ', $errors));
        }
        return $errors;
    }


}

